
<div class="col-xs-6">
<?php
    $postThumbnailUrl = get_the_post_thumbnail_url($post);
?>
<article class="blog-post">
    <a class="image" href="<?= get_the_permalink($post); ?>" style="<?= empty($postThumbnailUrl) ? "" : "background-image: url($postThumbnailUrl);" ?>">
        <?php
            if(count(get_the_category($post)) > 0)
                echo '<h6 class="category blog-post-cat">' . get_the_category($post)[0]->name . '</h6>';
        ?>
    </a>
    <div class="excerpt-wrapper blog-wrapper">
        <h2 class="title"><?= get_the_title($post); ?></h2>
        <div class="text-with-border">
            <?= get_the_date("F j, Y", $post); ?>
        </div>
        <h3 class="subtitle"><?= get_the_author($post); ?></h3>
        <div class="text">
            <?= get_the_excerpt($post); ?>
        </div>
    </div>
    <a href="<?= get_the_permalink($post); ?>" class="btn btn-red icon-btn blog-btn">Read More</a>
</article>
</div>
