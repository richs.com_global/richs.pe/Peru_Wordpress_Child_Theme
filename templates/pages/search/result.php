<?php
    $postThumbnailUrl = get_the_post_thumbnail_url($post);
?>
<article class="search-result">
    <a href="<?= get_the_permalink($post);?>"><h2 class="title"><?= get_the_title($post); ?></h2></a>
    <div class="text">
        <?= get_the_excerpt($post);?>
    </div>
    <a href="<?= get_the_permalink($post); ?>" class="btn btn-red icon-btn">Read More<i class="fas fa-chevron-right"></i></a>
</article>