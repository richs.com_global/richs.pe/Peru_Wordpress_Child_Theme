<?php if($block): ?>

     <?php
        if ($background = $block['background_image_right']) {
            $background_url_right = $background['url'];
        }
        if ($background = $block['background_image_left']) {
            $background_url_left = $background['url'];
        }
    ?>

    <div class="row content-wrapper">
        <div class="col-sm-6 background hero-style-left-icehot<?= $hero_style; ?>" style="background-image:url(<?= $background_url_left; ?>);">
            <div class="container-half-two">
                <div class="text-container">
                    <?php if (!empty($block['top_title'])) : ?>
                            <p class="top-title"><?= $block['top_title']; ?></p>
                    <?php endif; ?>

                    <?php if (!empty($block['title'])) : ?>
                        <h2 class="block-title"><?= $block['title']; ?></h2>
                    <?php endif; ?>

                    <?php if (!empty($block['copy'])) : ?>
                            <div class="copy"><?= $block['copy']; ?></div>
                    <?php endif; ?>

                    <?php if ($button = $block['button']) : ?>
                        <a class="btn btn-red icehot-red-btn" href="<?= $button['url']; ?>" target="<?= $button['target']; ?>"><?= $button['title']; ?></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="col-sm-6 content-wrapper background hero-style-right-icehot<?= $hero_style; ?>" style="background-image:url(<?= $background_url_right; ?>);">
            <div class="image-container container-half-one">
                <div class="img-container">
                    <img src="<?= $block['image']['url']; ?>" alt="<?= $block['image']['alt']; ?>" />
                </div>
            </div>
        </div>

    </div>

<?php endif; ?>