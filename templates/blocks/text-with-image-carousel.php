<?php if($block):
    $options = [
        'slidesToShow' => 1,
        'slidesToScroll' => 1,
        'prevArrow' => '<button type="button" class="slick-previous"><i class="fas fa-chevron-left"></i></button>',
        'nextArrow' => '<button type="button" class="slick-next"><i class="fas fa-chevron-right"></i></button>',
        'speed' => 800,
        'dots' => true,
        'touchThreshold' => 100,
    ];

    $slide_count = count($block['slides']);
?>
    <div class="container">
        <?php if($block['title'] != '') : ?>
        <h1 class="block-title mb-0"><?= $block['title']; ?></h1>
        <?php endif; ?>
        <slick-carousel :options="<?= htmlspecialchars(json_encode($options), ENT_QUOTES, 'UTF-8') ?>" class="carousel timed-carousel">
            <?php foreach($block['slides'] as $slide) : ?>
                <div class="item image-container-blck">
                    <div class="flex image_position-<?= $slide['image_position']; ?>">
                        <div class="image-container">
                            <img class="contact-tb" src="<?= $slide['image']['url']; ?>" alt="<?= $slide['image']['alt']; ?>" />
                        </div>
                        <div class="text-container">
                            <h1 class="block-title market-pro"><?= $slide['title']; ?></h1>
                            <div class="text"><?= $slide['text']; ?></div>
                            <?php if ($button = $slide['button']) : ?>
                                <a class="btn btn-red icon-btn image-carousel-btn" href="<?= $button['url']; ?>" target="<?= $button['target']; ?>"><?= $button['title']; ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </slick-carousel>
    </div>
<?php endif; ?>