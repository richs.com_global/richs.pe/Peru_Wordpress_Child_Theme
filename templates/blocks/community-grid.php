<div class="container">
    <h2 class="block-title"><?= $block['title']; ?></h2>
    <div class="flex">
        <?php foreach ($block['organisations'] as $organisation) : $organisationID = $organisation->ID; ?>
            <a class="organisation card" href="<?= get_permalink($organisationID); ?>" style="background-image:url(<?= get_field('main_image', $organisationID)['url']; ?>);">
                <div class="text-container relative">
                    <h2 class="name"><?= get_field('charity_title', $organisationID); ?></h2>
                    <div class="job-title"><?= get_field('charity_name', $organisationID); ?></div>
                </div>
                <div class="absolute bg-black overlay"></div>
            </a>        
        <?php endforeach; ?>
    </div>
</div>