<?php 

// vars
$countryCode = get_field('Country_Code');
$image1 = get_field('digital_recipe_image_1');
$image2 = get_field('digital_recipe_image_2');
$image3 = get_field('digital_recipe_image_3');
$recipeTag = get_field('recipe_tag');
$description = get_field('digital_recipe_description');
$servings = get_field('servings');
$prepTime = get_field('prep_time');
$cookingTime = get_field('cooking_time');
$difficulty = get_field('difficulty');

$ingredientList = get_field('ingredient_list');
$ingredientBullets = $ingredientList[0];

$method = get_field('method');
$methodList = $method[0];

$featuredProducts = get_field('featured_products');
$productList = $featured_products[0];


$relatedProducts = get_field("related_products");
$relatedProductsArr = $relatedProducts[0];


$servingSize = get_field('ServingSize');
$servingSizeUOM = get_field('ServingSizeUOM');
$shelfLifefromMfg = get_field('ShelfLifefromMfg');
$shelfLifeRefrigeratedPrepared = get_field('ShelfLifeRefrigeratedPrepared');
$shelfLifeUOM = get_field('ShelfLifeUOM');
$storageLocal = get_field('StorageLocal');
$allergens = get_field('allergens');
$allergensArr = json_decode($allergens, true);
$allergensFinalArray = array();
$packagings = get_field('packagings');
$packagingsArr = json_decode($packagings, true);
$claims = get_field('claims');
$claimsArr = json_decode($claims, true);
$categories = get_the_terms( $post->ID , 'recipes_categories' );
$parent = get_term_parents_list( $categories[0]->term_id, 'product_categories', array('link' => false) );
$parentName = explode("/", $parent);


// ---------------------- BEGINNING OF PAGE ------------------

get_header(); 

?>



<!-- <div id="crumb"> -->
<div class="product-breadcrumbs" id="crumb">
    <div class="container">
        <?php if ($countryCode === "IN") { ?>
            <a href="<?php echo get_home_url(); ?>/our-recipes"><i class="fas fa-chevron-left blog-fa-left"></i> Recipes</a>
        <?php

        } else if ($countryCode === "TR") { ?>
            <a href="<?php echo get_home_url(); ?>/our-recipes"><i class="fas fa-chevron-left blog-fa-left"></i> Recipes</a>
        <?php

        } else if ($countryCode === "PE") { ?>
            <a href="<?php echo get_home_url(); ?>/our-recipes"><i class="fas fa-chevron-left blog-fa-left"></i> Recipes</a>
        <?php

        } else if ($countryCode === "JA") { ?>
            <a href="<?php echo get_home_url(); ?>/our-recipes"><i class="fas fa-chevron-left blog-fa-left"></i> Recipes</a>
        <?php
        }

        else { ?>
            <a href="<?php echo get_home_url(); ?>/our-recipes"><i class="fas fa-chevron-left blog-fa-left"></i> Recipes</a>
        <?php } ?>


        <!-- <?php if($parentName !== null) {
            ?>
            / 
            <a href="<?php echo get_home_url() . "/product_categories/" .  $parentName[0]; ?>"><?php echo $parentName[0] ?></a> / 
            <a href="<?php echo get_home_url() . "/product_categories/" .  $parentName[1]; ?>"><?php echo $parentName[1] ?></a>
            <?php
        }
        ?> -->
    </div>
</div>
<!-- </div> -->



<section class="single-product-section single-recipe-container">
<div class="container single-product">
        <div class="single-product-grid">
            <div class="single-product-left">
                <div class="content-container row product-image"> 
                        <div id="slider-wrapper">
                        <div id="image-slider">
                            <ul>
                                <li class="active-img">
                                    <img src="<?php echo $image1; ?>" alt="" /> 
                                </li>
                                <li>
                                    <img src="<?php echo $image2; ?>" alt="" />
                                </li>
                                <li>
                                    <img src="<?php echo $image3; ?>" alt="" />
                                </li>											
                            </ul>
                        </div>
                        </div>

                </div>
            </div>
            <div class="single-product-right">
                <div class="content-container row">
                    <h2 class="product-title"><?php echo get_the_title($post_object->ID); ?></h2>
                    <p class="product-description"><?php echo $description; ?> </p>
                </div>
            
                <?php if( get_field('recipe_tag') ): ?>
                    <p class="recipe-tag" style="display: none;"><?php the_field('recipe_tag'); ?></p>
                <?php endif; ?>
                
                <div class="section-separator"></div>
                <div class="content-container row recipe-details">
                    <div class="left-col">

                    <?php if( get_field('servings') ): ?>
                        <div>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/Icons/user.svg" alt='placeholder' />
                            <p>
                                <?php if ($countryCode === "IN") { ?>
                                    <?php _e( 'SERVES' ); ?> 
                                <?php

                                } else if ($countryCode === "TR") { ?>
                                    <?php _e( 'SERVES' ); ?> 
                                <?php

                                } else if ($countryCode === "PE") { ?>
                                    <?php _e( 'SERVES' ); ?> 
                                <?php

                                } else if ($countryCode === "JA") { ?>
                                    <?php _e( 'SERVES' ); ?> 
                                <?php
                                }

                                else { ?>
                                    <?php _e( 'SERVES' ); ?> 
                                <?php } ?>

                                <span><?php the_field('servings'); ?></span></p>
                        </div>
                    <?php endif; ?>

                    <?php if( get_field('prep_time') ): ?>
                        <div>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/Icons/timer.svg" alt='placeholder' />
                            <p>
                                <?php if ($countryCode === "IN") { ?>
                                    <?php _e( 'PREP TIME' ); ?>
                                <?php

                                } else if ($countryCode === "TR") { ?>
                                    <?php _e( 'PREP TIME' ); ?>
                                <?php

                                } else if ($countryCode === "PE") { ?>
                                    <?php _e( 'PREP TIME' ); ?>
                                <?php

                                } else if ($countryCode === "JA") { ?>
                                    <?php _e( 'PREP TIME' ); ?>
                                <?php
                                }

                                else { ?>
                                    <?php _e( 'PREP TIME' ); ?>
                                <?php } ?>

                                 <span><?php the_field('prep_time'); ?></span></p>
                        </div>
                    <?php endif; ?>

                    <?php if ($cookingTime !== null ) { ?>
                        <div style="display: none;">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/Icons/cooking.svg" alt='placeholder' />
                            <p>
                                <?php if ($countryCode === "IN") { ?>
                                    <?php _e( 'COOKING TIME' ); ?>
                                <?php

                                } else if ($countryCode === "TR") { ?>
                                    <?php _e( 'COOKING TIME' ); ?>
                                <?php

                                } else if ($countryCode === "PE") { ?>
                                    <?php _e( 'COOKING TIME' ); ?>
                                <?php

                                } else if ($countryCode === "JA") { ?>
                                    <?php _e( 'COOKING TIME' ); ?>
                                <?php
                                }

                                else { ?>
                                    <?php _e( 'COOKING TIME' ); ?>
                                <?php } ?>

                                 <span><?php the_field('cooking_time'); ?></span></p>
                        </div>
                    <?php } ?>

                    <?php if ($difficulty !== "None"  ) { ?>
                         <div>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/Icons/icon1.svg" alt='placeholder' />
                            <p>
                                 <?php if ($countryCode === "IN") { ?>
                                    <?php _e( 'DIFFICULTY' ); ?>
                                <?php

                                } else if ($countryCode === "TR") { ?>
                                    <?php _e( 'DIFFICULTY' ); ?>
                                <?php

                                } else if ($countryCode === "PE") { ?>
                                    <?php _e( 'DIFFICULTY' ); ?>
                                <?php

                                } else if ($countryCode === "JA") { ?>
                                    <?php _e( 'DIFFICULTY' ); ?>
                                <?php
                                }

                                else { ?>
                                    <?php _e( 'DIFFICULTY' ); ?>
                                <?php } ?>
                                
                                 <span><?php the_field('difficulty'); ?></span></p>
                        </div>
                    <?php } else {
                        // Do nothing
                    } ?>
                        
                    </div>
                </div>

              <!--  Tags -->
                <div class="content-container row">
                    <?php $post_tags = get_the_tags();
                        if ( $post_tags ) { ?>
                            <div class="section-separator"></div> 
                            <p>
                                Tags: <?php
                            foreach( $post_tags as $tag ) {
                            echo $tag->name . ', '; 
                            }
                        }
                    ?></p>
                </div>

                <div class="section-separator"></div>
                <div class="content-container row">
                    <h4>FEATURED PRODUCTS</h4>
                    <div class="slider multiple-items"> <?php 
                        foreach ($relatedProducts as $relatedProduct) { 
                            foreach ($relatedProduct as $product) { 
                                $queried_post = get_post($product->ID);
                                $featured_img_url = get_the_post_thumbnail_url($product->ID,'full');
                                $pack_shot = get_field('Digital_Features_and_Benefits_1', $product->ID);
                       
                                ?>
                                <div>
                                    <a href="<?php echo get_home_url() . "/product/" . $product->post_name; ?>">
                                    <div class="image-container">
                                        <img src="<?php echo $pack_shot ?>" alt="product" />
                                    </div>
                                    <h4><?php echo $product->post_title; ?></h4>
                            </a>
                                </div> <?php
                            }
                        } ?> 
                    </div>
                </div>
            </div>
        </div>

        <!-- TABS -->

        <div class="single-product-tabs">
            <div id="tabs">
                <ul>
                    <li><a href="#tabs-1">
                         <?php if ($countryCode === "IN") { ?>
                            <?php _e( 'INGREDIENTS' );?> 
                        <?php

                        } else if ($countryCode === "TR") { ?>
                            <?php _e( 'INGREDIENTS' );?> 
                        <?php

                        } else if ($countryCode === "PE") { ?>
                            <?php _e( 'INGREDIENTS' );?> 
                        <?php

                        } else if ($countryCode === "JA") { ?>
                            <?php _e( 'INGREDIENTS' );?> 
                        <?php
                        }

                        else { ?>
                            <?php _e( 'INGREDIENTS' );?> 
                        <?php } ?>
                    </a></li>
                    <li><a href="#tabs-2">
                        <?php if ($countryCode === "IN") { ?>
                            <?php _e( 'METHOD' ); ?>
                        <?php

                        } else if ($countryCode === "TR") { ?>
                            <?php _e( 'METHOD' ); ?>
                        <?php

                        } else if ($countryCode === "PE") { ?>
                            <?php _e( 'METHOD' ); ?>
                        <?php

                        } else if ($countryCode === "JA") { ?>
                            <?php _e( 'METHOD' ); ?>
                        <?php
                        }

                        else { ?>
                            <?php _e( 'METHOD' ); ?>
                        <?php } ?>
                        </a></li>
                </ul>

                <div id="tabs-1">
                    <div class="row">
                        <div class="col-md-6">
                            <?php if( have_rows('ingredient_list') ): ?>
                                <ol>
                                    <?php while( have_rows('ingredient_list') ): the_row(); ?>
                                        <li><?php the_sub_field('ingredient_bullets'); ?></li>
                                    <?php endwhile; ?>
                                </ol>
                            <?php endif; ?>

                            <?php if( get_field('ingredient_editor') ): ?>
                                <div>
                                    <p><span><?php the_field('ingredient_editor'); ?></span></p>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="col-md-5">
                             <?php if( get_field('recipe_video') ): ?>
                                <div class="embed-container">
                                    <?php the_field('recipe_video'); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="col-md-5"></div>
                    </div>
                </div>

                <div id="tabs-2">
                    <?php if( have_rows('method') ): ?>
                        <ol>
                            <?php while( have_rows('method') ): the_row(); ?>
                                <li><?php the_sub_field('method_list'); ?></li>
                            <?php endwhile; ?>
                        </ol>
                    <?php endif; ?>
                    <?php if( get_field('method_editor') ): ?>
                        <div>
                            <p><span><?php the_field('method_editor'); ?></span></p>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <!-- <div class="row" style="display: none !important;">
            <?php
            $tips = get_field('tips');
            if( $tips ): ?>
                <div class="container row tips">
                <div class="col-md-1">
                    <img class="tip-image" src="<?php echo esc_url( $tips['tips_image']['url'] ); ?>" alt="<?php echo esc_attr( $tips['tips_image']['alt'] ); ?>"/>
                </div>
                <div class="col-md-11">
                    <div class="content">
                        <?php if ($countryCode === "IN") { ?>
                            <h3><b>TIPS</b></h3>
                        <?php

                        } else if ($countryCode === "TR") { ?>
                            <h3><b>TIPS</b></h3>
                        <?php

                        } else if ($countryCode === "PE") { ?>
                            <h3><b>TIPS</b></h3>
                        <?php

                        } else if ($countryCode === "JA") { ?>
                            <h3><b>TIPS</b></h3>
                        <?php
                        }

                        else { ?>
                            <h3><b>TIPS</b></h3>
                        <?php } ?>

                        <?php echo $tips['tips_text']; ?>
                        <a href="<?php echo esc_url( $tips['tips_link']['url'] ); ?>"><?php echo esc_html( $tips['tips_link']['title'] ); ?></a>
                    </div>
                </div>
                </div>
            <?php endif; ?>
        </div> -->

        <!-- <div class="row">
        <?php
            $tips = get_field('tips');
            if( $tips ): ?>
                <div id="tips" class="container row tips">
                    <div class="col-md-1">
                        <img class="tip-image" src="<?php echo esc_url( $tips['tips_image']['url'] ); ?>" alt="<?php echo esc_attr( $tips['tips_image']['alt'] ); ?>"/>
                    </div>
                    <div class="col-md-11">
                        <div class="content">
                        <h3><b>TIPS</b></h3>
                            <?php echo $tips['tips_text']; ?>
                            <a href="<?php echo esc_url( $tips['tips_link']['url'] ); ?>"><?php echo esc_html( $tips['tips_link']['title'] ); ?></a>
                        </div>
                    </div>
                </div>
                <style type="text/css">
                    #tips {
                        background-color: <?php echo esc_attr( $tips['color'] ); ?>;
                    }
                </style>
            <?php endif; ?>
        </div> -->

        <div class="row">


            <div class="related-products">
            <h2>RELATED RECIPES</h2>
            <div class="product-list">
            <?php 
             $categoryArgs = array(
                'post_type'   => 'recipes',
                'post_status' => 'publish',
                'posts_per_page' => 3,
                'post__not_in' => array( $post->ID ),
                'tax_query'   => array(
                    array(
                        'taxonomy' => 'recipes_categories',
                        'field'    => 'slug',
                        'terms'    => $categories[0]->slug
                    )
                )
            );
            $categoryRecipes = new WP_Query( $categoryArgs );
            while( $categoryRecipes->have_posts() ) :
                $categoryRecipes->the_post();
                ?>


                <?php if ( is_user_logged_in() ) { ?>
                <div class="product-list-item">
                    <a href="<?php echo get_the_permalink(); ?>">
                    <div class="image-container">
                    <?php if (has_post_thumbnail( $post->ID ) ) {
                        the_post_thumbnail(); 
                    } else { ?>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/placeholder.jpg" alt='placeholder' />
                        <?php
                    } ?>
                    </div>
                        <h3 class="product-title"><?php echo get_the_title(); ?></h3>
                        <?php $member_tags = get_the_terms( get_the_ID(), 'post_tag' ); 
                        foreach ( $member_tags as $tag) { 
                            if ($tag->name == "New") {?>
                                <div class="product-tag">
                                    <span><?php echo $tag->name; ?></span>
                                </div>
                                <?php
                            }
                        }
                        ?>     
                    </a>
                </div> 

                <?php } else { ?>

                    <div class="product-list-item">
                    <a  href="<?php echo get_home_url() . "/user/"; ?>">
                    <div class="image-container">
                    <img class="category-lock category-recipe-lock" src="<?php echo get_stylesheet_directory_uri(); ?>/recipe-includes/images/lock-alt-solid.svg" alt='placeholder' />
                    <?php if (has_post_thumbnail( $post->ID ) ) {
                        the_post_thumbnail(); 
                    } else { ?>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/placeholder.jpg" alt='placeholder' />
                        <?php
                    } ?>
                    </div>
                        <h3 class="product-title"><?php echo get_the_title(); ?></h3>
                        <?php $member_tags = get_the_terms( get_the_ID(), 'post_tag' ); 
                        foreach ( $member_tags as $tag) { 
                            if ($tag->name == "New") {?>
                                <div class="product-tag">
                                    <span><?php echo $tag->name; ?></span>
                                </div>
                                <?php
                            }
                        }
                        ?>     
                    </a>
                    </div> 
                <?php } ?>

            <?php
            endwhile;
            ?>
            </div> 
        </div>
        

        </div>  

</section>

<?php

get_footer();
