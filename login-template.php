
<?php

/**
 * Template Name: Login / Register Template
 *
 * This is the most generic template file in a WordPress theme
 * It is used to display a page when nothing more specific matches a query.
 *
 */		

get_header(); 

loop_posts(function() {
    render('page-content');
});

get_footer();


