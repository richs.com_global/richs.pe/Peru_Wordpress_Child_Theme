<?php

namespace Ix\Blocks;

/**
 * @package  WordPress Theme
 * @author   Benchmark Digital
 * @version  $Revision: 1.0.0
 */

use Cn\FieldGroup;
use Cn\Blocks\Block;

class IcehotFullWidthRightImage extends Block
{
    public function __construct()
    {
        parent::register_block(
            'icehot-full-width-right-image',
            [
                'title'           => 'Icehot Full Width Right Image',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['hero']
            ]
        );
    }
}