<?php

namespace Ix\Blocks;

/**
 * @package  WordPress Theme
 * @author   Benchmark Digital
 * @version  $Revision: 1.0.0
 */

use Cn\FieldGroup;
use Cn\Blocks\Block;

class IcehotBar extends Block
{
    public function __construct()
    {
        parent::register_block(
            'icehot-bar',
            [
                'title'           => 'Icehot Bar',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['banner']
            ]
        );
    }
}