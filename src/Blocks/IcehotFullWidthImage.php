<?php

namespace Ix\Blocks;

/**
 * @package  WordPress Theme
 * @author   Benchmark Digital
 * @version  $Revision: 1.0.0
 */

use Cn\FieldGroup;
use Cn\Blocks\Block;

class IcehotFullWidthImage extends Block
{
    public function __construct()
    {
        parent::register_block(
            'icehot-full-width-image',
            [
                'title'           => 'Icehot Full Width Image',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['hero']
            ]
        );
    }
}