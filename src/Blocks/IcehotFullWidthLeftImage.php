<?php

namespace Ix\Blocks;

/**
 * @package  WordPress Theme
 * @author   Benchmark Digital
 * @version  $Revision: 1.0.0
 */

use Cn\FieldGroup;
use Cn\Blocks\Block;

class IcehotFullWidthLeftImage extends Block
{
    public function __construct()
    {
        parent::register_block(
            'icehot-full-width-left-image',
            [
                'title'           => 'Icehot Full Width Left Image',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['hero']
            ]
        );
    }
}