<?php

namespace Ix\Acf\Blocks;

use Cn\Acf\FieldGroup;
use Cn\Acf\ReusableFields;
use StoutLogic\AcfBuilder\FieldsBuilder;

class IcehotPortfolioRange extends FieldGroup
{
    use ReusableFields;

    protected function build()
    {
        $this->setLocation('block', '==', 'acf/icehot-portfolio-range')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('icehot-portfolio-range');
        $fields
            ->addImage('background_image')
            ->addText('top_title')
            ->addText('title')
            ->addWysiwyg('copy')
            ->addRepeater('product_range_blocks')
                ->addColorPicker('background_color')
                ->addText('product_title')
                ->addImage('image')
            ->endRepeater()
            ->addLink('button');
        return $fields;
    }
}

